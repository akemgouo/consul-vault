package com.sid.consulvault;

import com.sid.consulvault.configs.ConsulConfig;
import com.sid.consulvault.configs.VaultConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
//@RefreshScope
public class ConsulConfigRestController {

    @Autowired
    private ConsulConfig consulConfig;

    @Autowired
    private VaultConfig vaultConfig;

    @GetMapping("/myConfig")
    public Map<String, Object> myConfig(){
        return Map.of("consulConfig",consulConfig, "vaultConfig", vaultConfig);
    }

}
